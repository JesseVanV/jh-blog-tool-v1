import { combineReducers } from 'redux';
import ReducerPosts from './reducer-posts';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  posts: ReducerPosts,
  form: formReducer  // make sure to use 'form' to get formreducer
});

export default rootReducer;
