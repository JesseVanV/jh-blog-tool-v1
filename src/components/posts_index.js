import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getPosts } from '../actions/index';
import { Link } from 'react-router';

class PostsIndex extends Component {

  componentWillMount() {  // lifecycle method loads before component added, only once
    this.props.getPosts();
  }

  //this will take the posts from props and create the li elements, using a key from the id, and the title and categories properties.
  renderPosts() {
    return this.props.posts.map((post) => {
      return (
        <li className="list-group-item" key={ post.id }>
          <Link to={ "/posts/" + post.id } className="post-list-link">
          <strong>{ post.title }</strong>
        <span className="post-list-category">{ post.categories }</span>
        </Link>
        </li>

    );
  });
}

  render() {
    return (
      <div>
        <div className="text-xs-right">
          <Link to="/post/new" className="btn btn-primary">+</Link>
        </div>
        <h1>Posts</h1>
        <ul className="list-group">
          { this.renderPosts() }
        </ul>
        </div>
    )
  }
}

function mapStateToProps(state) {
  return { posts: state.posts.all }
}

export default connect(mapStateToProps, { getPosts })(PostsIndex);
