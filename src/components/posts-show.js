import React, { Component, PropTypes } from 'react';
import { getSinglePost, deletePost } from '../actions/index';
import { connect } from 'react-redux';
import { Link } from 'react-router';

class PostsShow extends Component {

  static contextTypes = {
    router: PropTypes.object
  }

  componentWillMount() {
    this.props.getSinglePost(this.props.params.id);
  }

  onDeleteClick(props) {
    this.props.deletePost(this.props.params.id)
    .then(() => {
      this.context.router.push('/');
    });
  }

  render() {

    const { post } = this.props;

    //show a loader here until props.post is recieved
    if(!post) {
      return <div>Loading...</div>
    }
    
    return (
      <div>
        <Link to="/">Back</Link>
        <h3>{ post.title }</h3>
        <h5>{ post.categories }</h5>
        <p>{ post.content }</p>
        <button className="btn btn-danger pull-xs-right" onClick={ this.onDeleteClick.bind(this) }>Delete</button>
      </div>
    )
  }
}

function mapStatetoProps(state) {
  return { post: state.posts.post }
}

export default connect(mapStatetoProps, { getSinglePost, deletePost })(PostsShow);  //connect the action to the component
