import React, { Component } from 'react';

export default class App extends Component {
  render() { // this points to the routes.js child elements
    return (
      <div>
        { this.props.children }
      </div>
    );
  }
}
