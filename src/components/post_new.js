import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { createPost } from '../actions/index';
import { Link } from 'react-router';

class PostNew extends Component {
  static contextTypes = {
    router: PropTypes.object
  }
  onSubmit(props) {
    this.props.createPost(props)
    .then(() => {
      //blog post was save, navigate the user
      this.context.router.push('/');
    });
  }
  render() {
    const { fields: { title, categories, content }, handleSubmit } = this.props;  // ES6 for this.props.handleSubmit, and the title properties passed from fields.
    return (
      <div>
        <h3>Create a new post</h3>
        <form action="" onSubmit={ handleSubmit(this.onSubmit.bind(this)) }>
          <div className={ `form-group ${title.touched && title.invalid ? 'has-danger' : ''}`}>
            <label>Title</label>
            <input type="text" className="form-control" {...title} />
            <div className="text-help">
              { title.touched ? title.error : '' }
            </div>
          </div>
          <div className={ `form-group ${categories.touched && categories.invalid ? 'has-danger' : ''}`}>
            <label>Categories</label>
            <input type="text" className="form-control" {...categories} />
            <div className="text-help">
              { categories.touched ? categories.error : '' }
            </div>
          </div>
          <div className={ `form-group ${content.touched && content.invalid ? 'has-danger' : ''}`}>
            <label>Content</label>
            <textarea className="form-control" {...content} />
              <div className="text-help">
                { content.touched ? content.error : '' }
              </div>
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary">Save post</button>
            <Link to="/" className="btn btn-danger">Cancel</Link>
          </div>
        </form>
      </div>
    );
  }
}

//this is used in ReduxForm below, will match the errors property sith the fields in reduxForm • this will pass the input name with error as a property back
function validate(value) {
  const errors = {};
  if(!value.title) {
    errors.title = 'Enter a username';
  }
  if(!value.categories) {
    errors.categories = 'Enter one or more categories';
  }
  if(!value.content) {
    errors.content = 'Enter some content';
  }
  return errors;
}

export default reduxForm({
  form: 'PostNew',
  fields: [
    'title',
    'categories',
    'content'
  ],
  validate
}, null, { createPost })(PostNew);
